=======
Execute
=======

This is a collection of common patterns (and initially only one pattern!)
for running executables in a sub-process using the ``subprocess``
module.

For documentation, see `http://packages.python.org/execute`__

__ http://packages.python.org/execute
